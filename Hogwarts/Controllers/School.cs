﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Model.Entity;
using Model.Business;

namespace Hogwarts.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class School : ControllerBase
    {
        [HttpGet]
        public string Home()
        {
            return "Bienvenido al Colegio Hogwarts de Magia y Hechicería";
        }

        [HttpPost]
        public Message Insert(string Name, string Lastname, Int64 Identification, int Age, int House)
        {
            Data data = new Data
            {
                Name = Name,
                Lastname = Lastname,
                Identification = Identification,
                Age = Age,
                House = House
            };

           Request request = new Request();
           var result =  request.Insert(data);
            return result;
        }

        [HttpGet]
        public List<Consult> Consult(int? Id, string Name, string Lastname, int? Identification, int? Age, int? House)
        {
            Data data = new Data
            {
                Id = Id,
                Name = Name,
                Lastname = Lastname,
                Identification = Identification,
                Age = Age,
                House = House
            };

            Request request = new Request();
            var list = request.Consult(data);
            return list;
        }

        [HttpPost]
        public Message Update(int Id, string Name, string Lastname, Int64 Identification, int Age, int House)
        {
            Data data = new Data
            {
                Id = Id,
                Name = Name,
                Lastname = Lastname,
                Identification = Identification,
                Age = Age,
                House = House
            };

            Request request = new Request();
            var result = request.Update(data);
            return result;
        }

        [HttpPost]
        public Message Delete(int Id)
        {
            Request request = new Request();
            var result = request.Delete(Id);
            return result;
        }
    }
}
