﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Model.Entity;
using System.Data;
using System.Data.SqlClient;


namespace Model.Business
{
    public class Request
    {
        public Message Insert(Data data)
        {
            Message msg = new Message();
            try
            {
                int Row = 0;
                msg = Validations(data);
                if (msg.Status == 0)
                {
                    return msg;
                }
                msg = new Message();

                string path = System.IO.Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()) + @"\Model.Business\Database\Hogwarts.mdf";
                using (SqlConnection con = new SqlConnection(@"Data Source = (LocalDB)\MSSQLLocalDB; AttachDbFilename = " + path + "; Integrated Security = True"))
                {
                    using (SqlCommand cmd = new SqlCommand("InsertRequest", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@Name", SqlDbType.NVarChar).Value = data.Name;
                        cmd.Parameters.Add("@Lastname", SqlDbType.NVarChar).Value = data.Lastname;
                        cmd.Parameters.Add("@Identification", SqlDbType.Int).Value = data.Identification;
                        cmd.Parameters.Add("@Age", SqlDbType.Int).Value = data.Age;
                        cmd.Parameters.Add("@House", SqlDbType.NVarChar).Value = data.House;
                        cmd.Connection.Open();
                        Row = cmd.ExecuteNonQuery();
                    }
                }

                if (Row == 0)
                {
                    msg.Status = 0;
                    msg.Description = "No se pudo crear la solicitud";
                    return msg;
                }

                msg.Status = 1;
                msg.Description = "Se ha guardado la solicitud.";
                return msg;
            }
            catch (Exception EX)
            {
                msg.Status = 0;
                msg.Description = EX.Message;
                return msg;
            }
        }

        public Message Validations(Data data)
        {
            Message msg = new Message();
            if (string.IsNullOrEmpty(data.Name) || string.IsNullOrWhiteSpace(data.Name))
            {
                msg.Status = 0;
                msg.Description = "Ingrese el nombre";
                return msg;
            }

            if (!(data.Name.Length <= 20))
            {
                msg.Status = 0;
                msg.Description = "El nombre solo permite 20 caracteres";
                return msg;
            }

            if (string.IsNullOrEmpty(data.Lastname) || string.IsNullOrWhiteSpace(data.Lastname))
            {
                msg.Status = 0;
                msg.Description = "Ingrese el apellido";
                return msg;
            }

            if (!(data.Name.Length <= 20))
            {
                msg.Status = 0;
                msg.Description = "El apellido solo permite 20 caracteres";
                return msg;
            }

            if (data.Identification < 0)
            {
                msg.Status = 0;
                msg.Description = "la identificación no permite números negativos";
                return msg;
            }

            if (!(Convert.ToString(data.Identification).Length <= 10))
            {
                msg.Status = 0;
                msg.Description = "La identificación solo permite 10 digitos";
                return msg;
            }

            if (data.Age < 0)
            {
                msg.Status = 0;
                msg.Description = "La edad no permite números negativos";
                return msg;
            }

            if (!(Convert.ToString(data.Age).Length <= 2))
            {
                msg.Status = 0;
                msg.Description = "La edad solo permite 2 digitos";
                return msg;
            }

            string path = System.IO.Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()) + @"\Model.Business\Database\Hogwarts.mdf";
            using (SqlConnection con = new SqlConnection(@"Data Source = (LocalDB)\MSSQLLocalDB; AttachDbFilename = " + path + "; Integrated Security = True"))
            {
                using (SqlCommand cmd = new SqlCommand("HousesData", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@id", SqlDbType.Int).Value = data.House;
                    cmd.Connection.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (!reader.HasRows)
                        {
                            msg.Status = 0;
                            msg.Description = "La casa seleccionada no existe";
                            return msg;
                        }
                    }
                }
            }

            msg.Status = 1;
            msg.Description = "OK";
            return msg;
        }

        public List<Consult> Consult(Data data)
        {
            List<Consult> list = new List<Consult>();
            try
            {
                string path = System.IO.Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()) + @"\Model.Business\Database\Hogwarts.mdf";
                using (SqlConnection con = new SqlConnection(@"Data Source = (LocalDB)\MSSQLLocalDB; AttachDbFilename = " + path + "; Integrated Security = True"))
                {
                    using (SqlCommand cmd = new SqlCommand("ConsultRequest", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@id", SqlDbType.Int).Value = data.Id == null ? (object)DBNull.Value : data.Id;
                        cmd.Parameters.Add("@name", SqlDbType.NVarChar).Value = string.IsNullOrWhiteSpace(data.Name) ? (object)DBNull.Value : data.Name;
                        cmd.Parameters.Add("@lastname", SqlDbType.NVarChar).Value = string.IsNullOrWhiteSpace(data.Lastname) ? (object)DBNull.Value : data.Lastname;
                        cmd.Parameters.Add("@identification", SqlDbType.Int).Value = data.Identification == null ? (object)DBNull.Value : data.Identification;
                        cmd.Parameters.Add("@age", SqlDbType.Int).Value = data.Age == null ? (object)DBNull.Value : data.Age;
                        cmd.Parameters.Add("@house", SqlDbType.Int).Value = data.House == null ? (object)DBNull.Value : data.House;

                        cmd.Connection.Open();
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    Consult req = new Consult();
                                    req.Id = Convert.ToInt32(reader["Id"].ToString());
                                    req.Name = Convert.ToString(reader["Name"].ToString());
                                    req.Lastname = Convert.ToString(reader["Lastname"].ToString());
                                    req.Identification = Convert.ToInt32(reader["Identification"].ToString());
                                    req.Age = Convert.ToInt32(reader["Age"].ToString());
                                    req.HouseDescription = reader["HouseDescription"].ToString();
                                    list.Add(req);
                                }
                            }
                        }
                    }
                }
                return list;
            }
            catch (Exception EX)
            {
                return list = null;
            }
        }

        public Message Update(Data data)
        {
            Message msg = new Message();
            try
            {
                int Row = 0;
                msg = Validations(data);
                if (msg.Status == 0)
                {
                    return msg;
                }
                msg = new Message();

                string path = System.IO.Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()) + @"\Model.Business\Database\Hogwarts.mdf";
                using (SqlConnection con = new SqlConnection(@"Data Source = (LocalDB)\MSSQLLocalDB; AttachDbFilename = " + path + "; Integrated Security = True"))
                {
                    using (SqlCommand cmd = new SqlCommand("UpdateRequest", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@Id", SqlDbType.Int).Value = data.Id;
                        cmd.Parameters.Add("@Name", SqlDbType.NVarChar).Value = data.Name;
                        cmd.Parameters.Add("@Lastname", SqlDbType.NVarChar).Value = data.Lastname;
                        cmd.Parameters.Add("@Identification", SqlDbType.Int).Value = data.Identification;
                        cmd.Parameters.Add("@Age", SqlDbType.Int).Value = data.Age;
                        cmd.Parameters.Add("@House", SqlDbType.Int).Value = data.House;
                        cmd.Connection.Open();
                        Row = cmd.ExecuteNonQuery();
                    }
                }

                if (Row == 0)
                {
                    msg.Status = 0;
                    msg.Description = "No se pudo modificar la solicitud";
                    return msg;
                }

                msg.Status = 1;
                msg.Description = "Se ha modificado la solicitud " + data.Id;
                return msg;
            }
            catch (Exception EX)
            {
                msg.Status = 0;
                msg.Description = EX.Message;
                return msg;
            }
        }

        public Message Delete(int Id)
        {
            Message msg = new Message();
            try
            {
                int Row = 0;
                string path = System.IO.Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()) + @"\Model.Business\Database\Hogwarts.mdf";
                using (SqlConnection con = new SqlConnection(@"Data Source = (LocalDB)\MSSQLLocalDB; AttachDbFilename = " + path + "; Integrated Security = True"))
                {
                    using (SqlCommand cmd = new SqlCommand("DeleteRequest", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@Id", SqlDbType.Int).Value = Id;
                        cmd.Connection.Open();
                        Row = cmd.ExecuteNonQuery();
                    }
                }

                if (Row == 0)
                {
                    msg.Status = 0;
                    msg.Description = "No se pudo eliminar la solicitud";
                    return msg;
                }

                msg.Status = 1;
                msg.Description = "Se ha eliminado la solicitud " + Id;
                return msg;
            }
            catch (Exception EX)
            {
                msg.Status = 0;
                msg.Description = EX.Message;
                return msg;
            }
        }
    }
}
