﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Entity
{
    public class Message
    {
        public int Status { get; set; }
        public string Description { get; set; }
    }
}
