﻿using System;

namespace Model.Entity
{
    public class Data
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string Lastname { get; set; }
        public Int64? Identification { get; set; }
        public int? Age { get; set; }
        public int? House { get; set; }
    }

    public class Consult
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string Lastname { get; set; }
        public int? Identification { get; set; }
        public int? Age { get; set; }
        public string HouseDescription { get; set; }
    }
}
